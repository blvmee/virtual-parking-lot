public class Truck extends Vehicle {

    final String[] VARIETIES = {"MAN", "VOLVO", "SCANIA", "DAF", "KAMAZ", "MERCEDES-BENZ", "IVECO", "KENWORTH", "OPTIMUS PRIME"};

    public Truck() {
        super();
        this.iterationsLeft += 4;
        this.type = VARIETIES[randomize.nextInt(VARIETIES.length)];
    }
}
