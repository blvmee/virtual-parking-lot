import java.util.Random;

public class ParkingLot {

    static Random randomizer = new Random();

    int capacity;
    int freeSpots;
    Vehicle[] vehicles;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        freeSpots = capacity;
        vehicles = new Vehicle[capacity];
        System.out.println("~~ " + ANSI_YELLOW + "Created parking lot with " + capacity + " spots." + ANSI_RESET);
    }

    public void welcomeNewcomers() {
        if (freeSpots == 0) {
            System.out.println("~~ " + ANSI_RED + "No free spots! " + ANSI_YELLOW + "Next one comes in " + getMinIterationsLeft() + " iterations." + ANSI_RESET);
        } else {
            int newVehiclesAmount = 3 + randomizer.nextInt(capacity / 3);
            int newTrucksAmount = randomizer.nextInt(newVehiclesAmount/3);
            int newCarsAmount = newVehiclesAmount - newTrucksAmount;
            System.out.println("Generated " + newCarsAmount + " cars and " + newTrucksAmount + " trucks.");
            if (newCarsAmount != 0) welcomeCars(newCarsAmount);
            if (newTrucksAmount != 0) welcomeTrucks(newTrucksAmount);
        }
    }

    public void welcomeCars(int newCarsAmount) {
        int carsArrived = 0;
        for (int i = 0; (i < vehicles.length) && (carsArrived != newCarsAmount); i++) {
            if (vehicles[i] == null) {
                vehicles[i] = new Car();
                freeSpots--;
                carsArrived++;
            }
        }
        System.out.println("~~ " + ANSI_YELLOW + carsArrived + " cars have arrived." + ANSI_RESET);
    }

    public void welcomeTrucks(int newTrucksAmount) {
        int trucksArrived = 0;
        for (int i = 0; (i < vehicles.length - 1) && (trucksArrived != newTrucksAmount); i++) {
            if ((vehicles[i] == null) && (vehicles[i+1] == null)) {
                vehicles[i] = new Truck();
                vehicles[i+1] = vehicles[i];
                freeSpots -= 2;
                trucksArrived++;
            }
        }
        System.out.println("~~ " + ANSI_YELLOW + trucksArrived + " trucks have arrived." + ANSI_RESET);
    }
    public void banishExpiredVehicles() {
        if (capacity != freeSpots) {
            int vehiclesLeaving = 0;
            for (int i = 0; i < vehicles.length; i++) {
                if (vehicles[i] != null) {
                    if (i == 0) {
                        vehicles[i].reduceIteration();
                    } else {
                        if (vehicles[i] == vehicles[i-1]) { // i.e. is truck
                            continue;
                        } else {
                            vehicles[i].reduceIteration();
                        }
                    }
                }
                if ((vehicles[i] != null) && (vehicles[i].iterationsLeft <= 0)) {
                    vehicles[i] = null;
                    vehiclesLeaving++;
                    freeSpots++;
                }
            }
            if (vehiclesLeaving != 0) System.out.println("~~ " + ANSI_GREEN + vehiclesLeaving + " vehicles left the parking lot." + ANSI_RESET);
        }
    }

    public String showStatus() {
        if (freeSpots == capacity) {
            return ("Status:\n~~ " + ANSI_GREEN + "All " + capacity + " spots are free." + ANSI_RESET);
        }
        if (freeSpots == 0) {
            return ("Status:\n~~ " + ANSI_RED + "No free spots! " + ANSI_YELLOW + "Next one comes in " + getMinIterationsLeft() + " iterations." + ANSI_RESET);
        }
        return ("Status:\n~~ " + ANSI_YELLOW + freeSpots + " out of " + capacity + " spots are free.\n" + ANSI_RESET + "~~ " + ANSI_YELLOW + "Next free spot comes in " + getMinIterationsLeft() + " iterations." + ANSI_RESET);
    }

    public int getMinIterationsLeft() {
        int min = 2147483647; // vehicles[0].iterationsLeft may cause NullPointerException
        for (Vehicle vehicle : vehicles) {
            if ((vehicle != null) && (vehicle.iterationsLeft < min)) {
                min = vehicle.iterationsLeft;
            }
            if (min == 1) return 1;
        }
        return min;
    }

    public void cleanLot() {
        for (int i = 0; i < vehicles.length; i++) {
            vehicles[i] = null;
        }
        freeSpots = capacity;
        System.out.println("~~ " + ANSI_GREEN + "Parking is now completely free." + ANSI_RESET);
    }

    public void inspect() {
        for (int i = 0; i < vehicles.length; i++) {
            if (vehicles[i] != null) System.out.println("Slot [" + (i + 1) + "] = " + vehicles[i].toString());
        }
    }

    public boolean isEmpty() {
        return (freeSpots == capacity);
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";

}
