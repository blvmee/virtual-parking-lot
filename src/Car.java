public class Car extends Vehicle {

    final String[] VARIETIES = {"Mazda", "Kia", "Porsche", "Mercedes", "Renault", "Daewoo", "Lada", "Tesla", "Nissan", "Toyota", "Jaguar", "Bentley", "Lexus", "Subaru", "Lamborghini", "Audi", "Fiat", "Peugeot", "Chrysler", "Aston Martin", "Cadillac", "Volkswagen", "Chevrolet", "Subaru" , "Dodge", "Ford", "Mitsubishi", "Infiniti", "Honda", "Alfa Romeo", "Pontiac"};

    public Car() {
        super();
        this.type = VARIETIES[randomize.nextInt(VARIETIES.length)];
    }
}
