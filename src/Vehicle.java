import java.util.Random;

public abstract class Vehicle {

    Random randomize = new Random();

    int iterationsLeft = 2;
    int number;
    String type;
    String color;
    String[] colors = {ANSI_BLACK + "Black ", ANSI_RED + "Red ", ANSI_WHITE + "White ", ANSI_YELLOW + "Yellow ", ANSI_BLUE + "Blue ", ANSI_GREEN + "Green ", ANSI_PURPLE + "Purple ", ANSI_CYAN + "Cyan "};

    public Vehicle() {
        this.iterationsLeft += randomize.nextInt(10);
        this.number = 1000 + randomize.nextInt(8999);
        this.color = colors[randomize.nextInt(colors.length)];
    }

    public void reduceIteration() {
        this.iterationsLeft--;
    }

    @Override
    public String toString() {
        return (this.number + " " + this.color + this.type + ANSI_RESET + "; Leaves in " + this.iterationsLeft + " iterations.");
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_WHITE = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_BLACK = "\u001B[37m";

}
