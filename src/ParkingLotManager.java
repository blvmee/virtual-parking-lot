import java.util.Scanner;

public class ParkingLotManager {

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException {
        intro();
        int capacity;
        do {
            System.out.println("Enter parking lot capacity");
            capacity = scan.nextInt();
        } while (capacity < 3);
        ParkingLot parking = new ParkingLot(capacity);
        Thread.sleep(500);
        System.out.println("Commands:");
        System.out.println("'status/state' - see current state of parking lot;");
        System.out.println("'inspect' - inspect every vehicle;");
        System.out.println("'close' - close the parking lot;");
        System.out.println("'cls' - clear the parking;");
        System.out.println("Enter anything else to proceed with next move");
        System.out.println("------------Move 1-------------");
        int i = 0;
        boolean isClosing = false;
        while (true) {
            if (i == 0) bugfix();
            String input = "";
            if (!isClosing) {
                System.out.println("Waiting for your command..");
                input = scan.nextLine();
            } else {
                Thread.sleep(250);
            }
            switch (input) {
                case "status":
                case "state":
                    System.out.println(parking.showStatus());
                    break;
                case "inspect":
                    parking.inspect();
                    break;
                case "close":
                    System.err.println("Parking lot is closing...\n");
                    Thread.sleep(500);
                    isClosing = true;
                    break;
                case "cls":
                    parking.cleanLot();
                    i++;
                    System.out.println("------------Move " + (i+1) + "-------------");
                    break;
                default:
                    parking.banishExpiredVehicles();
                    if (!isClosing) parking.welcomeNewcomers();
                    i++;
                    System.out.println("------------Move " + (i+1) + "-------------");
                    break;
            }
            if (isClosing & parking.isEmpty()) {
                System.err.println("\nParking lot is closed for today.");
                Thread.sleep(2000);
                break;
            }
        }
    }

    static void bugfix() {
        try {
            int foo = Integer.parseInt(scan.nextLine());
        } catch (NumberFormatException ignore) {}
    }

    static String[] introColors = {"\u001B[31m","\u001B[33m","\u001B[32m","\u001B[34m","\u001B[35m"};

    static void intro() throws InterruptedException {
        char[] intro = "     Solevarnya Inc. 2020.  \n\n===== Virtual Parking Lot =====".toCharArray();
        for (int i = 0; i < intro.length; i++) {
            System.out.print(introColors[i%introColors.length]);
            System.out.print(intro[i]);
            System.out.print("\u001B[0m");
            if(((intro[i] != ' ') && (intro[i] != '=')) && (i < 27)) Thread.sleep(250);
        }
        System.out.println("\n");
    }

}
